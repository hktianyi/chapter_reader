# 初次使用，如果默认英文显示，先在【插件设置】设置中文显示

# 菜单位置（IDEA顶部菜单栏 / Tools / Read）
![输入图片说明](images/cdzs.png)

# 阅读模式说明：
## 常见问题
![输入图片说明](images/wt1.png)

## 【普通】阅读模式

优点：隐蔽

缺点：字体小（可以通过调整IDEA界面字体改善）

显示位置：

旧版UI默认展示在左下角
### 如果左下角不展示内容
1. 新版本: 先点击右上角Notifications，再看左下角是否显示，然后关闭Notifications窗口就可以正常显示了（2022.1 / 2023.1版本尝试后均可正常显示）

2. 旧版本：不知道有没有这种情况，也可以使用相同的办法。点击Event Log

3. 新版本左下角默认显示的类导航，右键底部的标签栏，第二个 Navigation Bar 选择 Top 即可跟旧版本一样

4. 或者在 插件设置 中使用 导航栏阅读 模式
![输入图片说明](images/zxjzs.png)


## 【全章节】阅读模式

优点：一次阅读整个章节、字体大小可调整

缺点：不隐蔽

显示位置：IDEA右侧工具栏【Read】
![输入图片说明](images/qzjzs.png)
小框位置，可以增加隐蔽性
![输入图片说明](images/qzjyd.jpg)


# 在线阅读：
```
首先感谢各位道友适配的各个在线网站！
再次强调各位初次使用的道友，同一个网站无需重复配置。如果网站结构变动，修改之前的配置即可。
```
怎么适配网站？详见：[在线阅读配置说明](https://gitee.com/wind_invade/chapter_reader/blob/master/%E5%9C%A8%E7%BA%BF%E9%98%85%E8%AF%BB%E7%BD%91%E7%AB%99%E9%80%82%E9%85%8D.MD)

## 操作方式：
1. 点击：【正常网站列表】选择显示出来的任意网站，会自动打开浏览器访问。
![输入图片说明](images/zcwzlb.png)

2. 在网站里找自己想看的小说，复制【章节目录】URL。
![输入图片说明](images/zjml.png)

3. 点击：【在线阅读】输入【章节目录】URL
![输入图片说明](images/ycyd.png)

4. 开始解析

5. 开始阅读



## 常见问题：
 1. 点击【正常网站列表】为什么一直卡住，没有任何网站？

    因为需要批次的，根据网站配置判断网站是否能正常阅读；

    默认超时是10秒，所以会有这种现象。可以尝试多等待一会。


 2. 为什么【章节】或者【内容】解析失败？

    a. 检查URL是不是【章节目录】的；


    b. 可能网站结构变化了，需要重新调整该网站配置。


 3. 为什么内容没显示完整（网站一个章节有多页）？


    查看内容是否存在【下一页】如果存在则在网站结构配置中新增【nextUrlPath】


 4. 按【下一页】直接跳章节了？内容节点有误。



# 本地阅读：
本地分为txt、epub两种文件阅读

 **其中txt文件编码必须为UTF-8** 

## 操作方式：
1. txt可以使用【简易文章阅读模式】快速测试效果，不需要任何额外操作
![输入图片说明](images/jywzyd.png)

2. 当txt文件正常读取后，可以尝试使用【本地阅读模式】，可以【章节跳转】需要配置文章标题的正则
![输入图片说明](images/zjyd.png)

正则示例：
```
默认正则是：【第.*章.*】，如果章节标题一致，则无需修改。
【第1238章 xxxxxx标题】正则：【第.*章.*】
【第一卷 第2章 xxxxxx标题】正则：【第.*章.*】，展示第几卷【.*第.*章.*】
【第一章 xxxxxx标题】正则：【第.*章.*】
【第1篇 xxxxxx标题】正则：【第.*篇.*】
反正自行找个在线正则调试的网站，很快即可成功。

注：有些小说开头或结尾，会有些与小说正文无关的内容。
    如【该句子 引用xxx作者 第x章 xxxx】这样的内容；
    根据正则会把这句话识别为一个章节标题，就会导致内容错乱。
例如下图:
解决：加上【^】开头，默认正则举例：【^第.*篇.*】这样就只会识别【第】开头的内容。
```
3. 开始解析

4. 开始阅读

## 常见问题：
 1. 章节标题识别不到？

    a. 正则表达式输入是否正确

    b. txt文件编码是否为【UTF-8】

 2. 按【下一页】直接跳章节了？正则无问题的情况下，直接加群把文件发给我复现。


## 其它模式描述
### 阅读模式菜单【EPUB阅读模式】或【EPUB全章节阅读模式】：支持查看章节列表 / 跳转章节
### 阅读模式菜单【简易文本阅读模式】：仅按【系统换行符，如：\r\n】分行，【插件设置 / 单行展示最大字数】无效，
### 阅读模式菜单【简易文章阅读模式】：仅按照【插件设置】 > 【设置单行展示最大字数】的值，进行内容分割。


# 怎么知道已经解析完了？
查看打印信息：

旧版IDEA【Event Log（默认在右下角工具栏）】

新版IDEA【Notifications（默认在右侧工具栏）】
![输入图片说明](images/jzwc.png)

# 阅读

点击菜单【下一页】 或使用 【快捷键】
![输入图片说明](images/xyy.png)